import express from 'express';

import categories from './api/categories';


const app = express();


app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', 'exp://localhost:19000');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
  res.setHeader('Access-Control-Allow-Credentials', true);
  next();
});


// Endpoints
app.use('/categories', categories);


app.get('/', (req, res) => res.send('Hello World!'));

app.listen(3000, () => console.log('Example app listening on port 3000!')); // eslint-disable-line no-console
