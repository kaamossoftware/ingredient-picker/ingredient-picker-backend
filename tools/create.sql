CREATE TABLE user (
    id VARCHAR(200) NOT NULL,
    email VARCHAR(100) NOT NULL, 
    PRIMARY KEY (id)
);

CREATE TABLE category (
    id INT AUTO_INCREMENT NOT NULL,
    name VARCHAR(50),
    user_id VARCHAR(200) NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (user_id) REFERENCES user(id)
);

CREATE TABLE ingredient (
    id INT AUTO_INCREMENT NOT NULL,
    name VARCHAR(150) NOT NULL,
    category_id INT NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (category_id) REFERENCES category(id)
);